@extends('layouts.public', [
'wsecond_title' => 'waqFraiser™',
'menu_active' => 'waqFraiser™',
'scrolled_navbar' => false
])

@section('css_plugins')
<link href="{{ mix('assets/cms/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css') }}" rel="stylesheet">
<!-- Sweetalert -->
<link rel="stylesheet" href="{{ mix('assets/cms/plugins/sweetalert2/sweetalert2.css') }}">
<!-- Select2 -->
<link href="{{ mix('assets/cms/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/quiz.css">
<link href="{{ mix('assets/cms/plugins/select2/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
<style type="text/css">
        .container, .container-lg, .container-md, .container-sm, .container-xl {
            max-width: 1366px;
        }
    </style>
<section class="ftco-section" id="waqf-section" style="padding:7em 0 2rem 0 !important">
    <div class="waqf-item py-6 jumbotron">
        <div class="container">
            <h2 class="section-title">KUESIONER AFTER ZOOMINAR WAQFRAISER</h2>
            <h3 class="section-description">Assalamualaikum wr.wb
            Terimakasih atas kehadirannya dalam Zoominar Crowdfunding 5.0"WAQFRAISER-Digital Syiar| digital income,kembali kami mohon partisipasinya untuk mengisi kuesioner responden berikut dan mengirimkan kembali kepada kami.Syukran Katsiran-Waalaikumslam wr.wb</h3>
            {{-- <p class="section-description">Isi/Pilihlah data diri Anda pada kolom yang tertera dibawah ini :<a href="{{ route('public.faq-waqfraiser.index') }}">WaqFraiser™ FAQ</a>Jenis Kelamin</p> --}}
        <div class="container">
            <h2 class="section-description"></h2>
        </div>
        </div>
    </div> 
    <form class="waqf-item" method="POST" action="" enctype="multipart/form-data">
        @csrf 
</body>

<div class="container">
    @if(Session::get('message'))
    <div class="alert alert-{{ Session::get('status') ?? 'info' }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5>
            @if(Session::get('message_icon'))
            <i class="icon fas fa-{{ Session::get('message_icon') ?? 'info' }}"></i>
        @endif {{ Session::get('status') ? ucwords(Session::get('status')) : 'Info' }}!</h5>
        {{ Session::get('message') }}
    </div>
    @endif
    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="duta_name" class="form-control @error('duta_name') is-invalid @enderror" id="input-duta_name" value="{{ old('duta_name') }}" placeholder="Nama Lengkap (Sesuai KTP)">
        @error('duta_name')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input type="text" name="duta_birth" class="form-control datetimepicker-input @error('duta_birth') is-invalid @enderror" id="input-duta_birth" data-toggle="datetimepicker" data-target="#input-duta_birth" placeholder="Tanggal Lahir"/>
        @error('duta_birth')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Tempat Lahir</label>

        <input type="hidden" name="birth_name" value="{{ old('birth_name') }}" id="input-birth_name">
        <input type="hidden" name="birth_old" value="{{ old('birth_old') }}" id="input-birth_old">

        <select class="form-control @error('duta_birth_place') is-invalid @enderror" id="input-duta_birth_place" name="duta_birth_place"></select>
        @error('duta_birth_place')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>No. KTP</label>
        <input type="text" name="duta_ktp" class="form-control @error('duta_ktp') is-invalid @enderror" id="input-duta_ktp" value="{{ old('duta_ktp') }}" placeholder="No. KTP">
        @error('duta_ktp')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Upload KTP</label>
        <div class="custom-file">
            <input type="hidden" name="duta_ktp_file" readonly="">
            <input type="file" class="custom-file-input @error('duta_ktp_file') is-invalid @enderror" name="duta_ktp_file" id="input-duta_ktp_file" accept=".jpg,.jpeg,.png">
            <label class="custom-file-label" for="input-duta_ktp_file">Choose file</label>

            @error('duta_ktp_file')
            <div class='invalid-feedback'>{{ $message }}</div>
            @enderror
            <small class="text-muted">JPEG/PNG, max: 1MB Filesize</small>
        </div>
    </div>
    <div class="form-group">
        <label>No. Telp (Whatsapp)</label>
        <input type="text" name="duta_phone" class="form-control @error('duta_phone') is-invalid @enderror" id="input-duta_phone" value="{{ old('duta_phone') }}" placeholder="No. Telp/Whatsapp">
        @error('duta_phone')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" name="duta_email" class="form-control @error('duta_email') is-invalid @enderror" id="input-duta_email" value="{{ old('duta_email') }}" placeholder="E-mail">
        @error('duta_email')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group">
        <label>Pendidikan Terakhir</label>
        <select class="form-control @error('duta_last_education') is-invalid @enderror" id="input-duta_last_education" name="duta_last_education">
            <option value="">- Pilih Jenjang Pendidikan Terakhir -</option>
            <option value="sd" {{ old('duta_last_education') == 'sd' ? 'selected' : '' }}>SD</option>
            <option value="smp" {{ old('duta_last_education') == 'smp' ? 'selected' : '' }}>SMP</option>
            <option value="sma/smk" {{ old('duta_last_education') == 'sma/smk' ? 'selected' : '' }}>SMA/SMK</option>
            <option value="diploma_i" {{ old('duta_last_education') == 'diploma_i' ? 'selected' : '' }}>DIPLOMA I</option>
            <option value="diploma_ii" {{ old('duta_last_education') == 'diploma_ii' ? 'selected' : '' }}>DIPLOMA II</option>
            <option value="diploma_iii" {{ old('duta_last_education') == 'diploma_iii' ? 'selected' : '' }}>DIPLOMA III</option>
            <option value="diploma_iv" {{ old('duta_last_education') == 'diploma_iv' ? 'selected' : '' }}>DIPLOMA IV</option>
            <option value="strata_i" {{ old('duta_last_education') == 'strata_i' ? 'selected' : '' }}>STRATA I</option>
            <option value="strata_ii" {{ old('duta_last_education') == 'strata_ii' ? 'selected' : '' }}>STRATA II</option>
            <option value="strata_iii" {{ old('duta_last_education') == 'strata_iii' ? 'selected' : '' }}>STRATA III</option>
            <option value="strata_iv" {{ old('duta_last_education') == 'strata_iv' ? 'selected' : '' }}>STRATA IV</option>
        </select>
        @error('duta_last_education')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Penghasilan Perbulan。</label>
        <select name="duta_job" class="form-control @error('duta_job') is-invalid @enderror" id="input-duta_job">
            <option value="">- Penghasilan Perbulan -</option>
            <option value="guru/pengajar" {{ old('duta_job') == 'guru/pengajar' ? 'selected' : '' }}>1.000.000</option>
            <option value="mahasiswa" {{ old('duta_job') == 'mahasiswa' ? 'selected' : '' }}>2.000.000</option>
            <option value="keuangan" {{ old('duta_job') == 'keuangan' ? 'selected' : '' }}>3.000.000</option>
            <option value="marketing" {{ old('duta_job') == 'marketing' ? 'selected' : '' }}>>5.000.000</option>
        </select>
        @error('duta_job')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>#KUIS</label>
    <div class="form-group">
        <label>Apakah anda sudah paham tentang Purwakafan?</label>
        <br>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">a.Belum terlalu paham</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">b.Butuh informasi lagi</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">c.Sudah paham</label>
</div>
</div>
<div class="form-group">
        <label>Apakah anda ingin ketahui lagi tentang WAQFRAISER?</label>
        <br>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">a.cara melakukan pendaftaran</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">b.Langkah-langkah kerjanya</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">c.Perhitungan kompensasinya</label>
</div>
</div>
     <div class="form-group">
        <label>Apakah anda memiliki kenalan yang akan diajak BERWAKAF?</label>
        <br>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">a.Tidak ada</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">b.Ada beberapa</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">c.Cukup banyak</label>
</div>
</div> 
     <div class="form-group">
        <label>Apakah anda punya kenalan yang cocok menjadi WAQFRAISER?</label>
        <br>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">a.Belum ada</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">b.Ada beberapa</label>
</div>
    <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">c.Cukup banyak</label>
</div>
</div>
    <div class="form-group">
        <label>Alasan Ingin Menjadi Waqua</label>
        <textarea class="form-control @error('duta_reason') is-invalid @enderror" name="duta_reason" id="input-duta_reason" placeholder="Tuliskan alasan anda ingin menjadi Waqua">{!! old('duta_reason') !!}</textarea>
        @error('duta_reason')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group">
        <label>Bersedia mengikuti Briefing Online pada:</label>
        <div class="pa-radio">
            <div class="pa-radio-item">
                <input type="radio" name="duta_brief" value="this_week" id="radio1" {{ old('duta_brief') ? (old('duta_brief') == 'this_week' ? 'checked' : '') : 'checked' }}>
                <label for="radio1">Minggu Ini</label>
            </div>
            <div class="pa-radio-item">
                <input type="radio" name="duta_brief" value="next_week" id="radio2" {{ old('duta_brief') == 'next_week' ? 'checked' : '' }}>
                <label for="radio2">Minggu Depan</label>
            </div>
            <div class="pa-radio-item">
                <input type="radio" name="duta_brief" value="next_month" id="radio3" {{ old('duta_brief') == 'next_month' ? 'checked' : '' }}>
                <label for="radio3">Bulan Depan</label>
            </div>
        </div>
    </div>

    <div class="mb-2">
        <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>
        @if($errors->has('g-recaptcha-response'))
        <span class="invalid-feedback" style="display: block;">
            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
        </span>
        @endif
    </div>

    <div class="btn-group float-right">
        <button type="button" id="btn-reset" class="btn btn-sm btn-danger">Reset</button>
        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
    </div>
</div>
</form>
</section>
@endsection

@section('js_plugins')
<script src="{{ mix('assets/cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script src="{{ mix('assets/cms/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<!-- Sweetalert -->
<script src="{{ mix('assets/cms/plugins/sweetalert2/sweetalert2.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Select2 -->
<script src="{{ mix('assets/cms/plugins/select2/js/select2.full.js') }}"></script>
@endsection


@section('js_inline')
<script>
    $(document).ready(() => {
        bsCustomFileInput.init();
        $('#input-duta_birth').datetimepicker({
            format: 'MMMM Do, YYYY',
            @if(old('duta_birth'))
            defaultDate: moment().format("{{ date('Y-m-d', strtotime(old('duta_birth'))) }}", 'MMMM Do, YYYY')
            @endif
        });

        let select2_query = {};
        $("#input-provinsi_id").select2({
            placeholder: 'Search Province',
            theme: 'bootstrap4',
            allowClear: true,
            ajax: {
                url: "",
                delay: 250,
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data, params) {
                    var items = $.map(data.data, function(obj){
                        obj.id = obj.id;
                        obj.text = obj.name;

                        return obj;
                    });
                    params.page = params.page || 1;

                    // console.log(items);
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: items,
                        pagination: {
                            more: params.page < data.last_page
                        }
                    };
                },
            },
            templateResult: function (item) {
                // console.log(item);
                // No need to template the searching text
                if (item.loading) {
                    return item.text;
                }
                
                var term = select2_query.term || '';
                var $result = markMatch(item.text, term);

                return $result;
            },
            language: {
                searching: function (params) {
                    // Intercept the query as it is happening
                    select2_query = params;
                    
                    // Change this to be appropriate for your application
                    return 'Searching...';
                }
            }
        });
        $("#input-provinsi_id").change((e) => {
            // console.log("Provinces is being changed");
            let provinsi = $(e.target).val();
            let select_data = $(e.target).select2('data');
            // console.log("Selected Province: "+provinsi);

            if(provinsi != null){
                $("#input-kabupaten_id option").remove();
                $("#input-kabupaten_id").attr('disabled', false).trigger('change');

                $("#input-provinsi_name").val(select_data[0]['text']);
                $("#input-provinsi_old").val(select_data[0]['id']);
            } else {
                $("#input-kabupaten_id option").remove();
                $("#input-kabupaten_id").attr('disabled', true).trigger('change');

                $("#input-provinsi_name").val('');
                $("#input-provinsi_old").val('');
            }
        });
        $("#input-kabupaten_id").select2({
            placeholder: 'Search Regency',
            theme: 'bootstrap4',
            allowClear: true,
            ajax: {
                url: "",
                delay: 250,
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                        province_id: $("#input-provinsi_id").val()
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data, params) {
                    var items = $.map(data.data, function(obj){
                        obj.id = obj.id;
                        obj.text = obj.name;

                        return obj;
                    });
                    params.page = params.page || 1;

                    // console.log(items);
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                         results: items,
                        pagination: {
                            more: params.page < data.last_page
                        }
                    };
                },
            },
            templateResult: function (item) {
                // console.log(item);
                // No need to template the searching text
                if (item.loading) {
                    return item.text;
                }
                
                var term = select2_query.term || '';
                var $result = markMatch(item.text, term);

                return $result;
            },
            language: {
                searching: function (params) {
                    // Intercept the query as it is happening
                    select2_query = params;
                    
                    // Change this to be appropriate for your application
                    return 'Searching...';
                }
            }
        });
        $("#input-kabupaten_id").change((e) => {
            // console.log("Regencies is being changed");
            let kabupaten = $(e.target).val();
            let select_data = $(e.target).select2('data');
            // console.log("Selected Regency: "+kabupaten);

            if(kabupaten != null){
                $("#input-kabupaten_name").val(select_data[0]['text']);
                $("#input-kabupaten_old").val(select_data[0]['id']);

                @if(empty($duta_wakaf))
                $("#input-duta_refcode").attr('disabled', false);
                $("#input-duta_refcode option").remove().trigger('change');
                @endif
            } else {
                $("#input-kabupaten_name").val('');
                $("#input-kabupaten_old").val('');

                @if(empty($duta_wakaf))
                $("#input-duta_refcode option").remove();
                $("#input-duta_refcode").attr('disabled', true).trigger('change');
                @endif
            }
        });
        $("#input-duta_birth_place").select2({
            placeholder: 'Birth Place',
            theme: 'bootstrap4',
            allowClear: true,
            ajax: {
                url: "",
                delay: 250,
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data, params) {
                    var items = $.map(data.data, function(obj){
                        obj.id = obj.id;
                        obj.text = obj.name;

                        return obj;
                    });
                    params.page = params.page || 1;

                    // console.log(items);
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: items,
                        pagination: {
                            more: params.page < data.last_page
                        }
                    };
                },
            },
            templateResult: function (item) {
                // console.log(item);
                // No need to template the searching text
                if (item.loading) {
                    return item.text;
                }
                
                var term = select2_query.term || '';
                var $result = markMatch(item.text, term);

                return $result;
            },
            language: {
                searching: function (params) {
                    // Intercept the query as it is happening
                    select2_query = params;
                    
                    // Change this to be appropriate for your application
                    return 'Searching...';
                }
            }
        });
        $("#input-duta_birth_place").change((e) => {
            // console.log("Regencies is being changed");
            let kabupaten = $(e.target).val();
            let select_data = $(e.target).select2('data');
            // console.log("Selected Regency: "+kabupaten);

            if(kabupaten != null){
                $("#input-birth_name").val(select_data[0]['text']);
                $("#input-birth_old").val(select_data[0]['id']);
            } else {
                $("#input-birth_name").val('');
                $("#input-birth_old").val('');
            }
        });
        $("#input-duta_refcode").select2({
            placeholder: 'Search Referal Code',
            theme: 'bootstrap4',
            allowClear: true,
            ajax: {
                url: "",
                delay: 250,
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                        province: $("#input-provinsi_id").val(),
                        regencies: $("#input-kabupaten_id").val(),
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data, params) {
                    var items = $.map(data.data, function(obj){
                        // console.log(obj);

                        obj.id = obj.duta_refcode;
                        obj.text = obj.duta_name;

                        if(!($.isEmptyObject(obj.regencies))){
                            obj.text += ' / '+obj.regencies.name;
                        } else {
                            obj.text += ' / -';
                        }

                        if(!($.isEmptyObject(obj.provinces))){
                            obj.text += ' / '+obj.provinces.name;
                        } else {
                            obj.text += ' / -';
                        }

                        return obj;
                    });
                    params.page = params.page || 1;

                    // console.log(items);
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: items,
                        pagination: {
                            more: params.page < data.last_page
                        }
                    };
                },
            },
            templateResult: function (item) {
                // console.log(item);
                // No need to template the searching text
                if (item.loading) {
                    return item.text;
                }
                
                var term = select2_query.term || '';
                var $result = markMatch(item.text, term);

                return $result;
            },
            language: {
                searching: function (params) {
                    // Intercept the query as it is happening
                    select2_query = params;
                    
                    // Change this to be appropriate for your application
                    return 'Searching...';
                }
            }
        });
        $("#input-duta_refcode").change((e) => {
            // console.log("Referal is being changed");
            let refcode = $(e.target).val();
            let select_data = $(e.target).select2('data');
            // console.log("Selected Refcode: "+refcode);

            if(refcode != null){
                $("#input-ref_name").val(select_data[0]['text']);
                $("#input-ref_old").val(select_data[0]['id']);
            } else {
                $("#input-ref_name").val('');
                $("#input-ref_old").val('');
            }
        });
        @if(old('provinsi_id') && old('provinsi_id') != 'null')
        // Old Data Provinsi
        var $newOption = $("<option selected='selected'></option>").val($("#input-provinsi_old").val()).text($("#input-provinsi_name").val());
        $("#input-provinsi_id").append($newOption).trigger('change');
        @endif
        @if(old('kabupaten_id') && old('kabupaten_id') != 'null')
        // Old Data Kabupaten
        var $newOption = $("<option selected='selected'></option>").val($("#input-kabupaten_old").val()).text($("#input-kabupaten_name").val());
        $("#input-kabupaten_id").append($newOption).trigger('change');
        @endif
        @if(old('duta_birth_place') && old('duta_birth_place') != 'null')
        // Old Data Birth Place
        var $newOption = $("<option selected='selected'></option>").val($("#input-birth_old").val()).text($("#input-birth_name").val());
        $("#input-duta_birth_place").append($newOption).trigger('change');
        @endif
        @if(old('duta_refcode') && old('duta_refcode') != 'null')
        // Old Data Birth Place
        var $newOption = $("<option selected='selected'></option>").val($("#input-ref_old").val()).text($("#input-ref_name").val());
        $("#input-duta_refcode").append($newOption).trigger('change');
        @endif
    });

$("#btn-reset").click((e) => {
    e.preventDefault();

    Swal.fire({
        title: 'Are you sure?',
        text: "All unsaved changes will be discarded!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, reset it!',
        showLoaderOnConfirm: true,
        cancelButtonText: 'No, cancel!',
        reverseButtons: true,
    }).then((result) => {
        location.reload(true);
    });
});
</script>
@endsection